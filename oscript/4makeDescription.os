#Использовать logos
#Использовать fs
#Использовать json
#Использовать params
#Использовать merge
#Использовать v8runner

Перем Лог;
Перем ПарсерJSON;
Перем Параметры;
Перем ПараметрыКонфигураций;

Перем ЗначенияПодстановки;

Перем КаталогРелиза;
Перем КаталогФайловКомплектаУстановки ;
Перем КаталогФайловКомплектаОбновления;
Перем КаталогФайловКомплектаПоставки  ;

Перем КаталогШаблонов;
Перем КаталогШаблоновКомплектаУстановки ;
Перем КаталогШаблоновКомплектаОбновления;
Перем КаталогШаблоновКомплектаПоставки  ;

///////////////////////////////////////////////////
// Основные шаги

Процедура ПодготовитьОписаниеКомплектаУстановки()

	// Параметры["БазаПроверкиУстановки.КаталогВыгрузкиФайловКонфигурации"],
	Лог.Информация("Начало подготовки описания комплекта установки " + ТекущаяДата());
	ФС.КопироватьСодержимоеКаталога(КаталогШаблоновКомплектаУстановки, КаталогФайловКомплектаУстановки);

	ФайлыДляПодстановкиПараметров = Новый Массив();
	ФайлыДляПодстановкиПараметров.Добавить("Методика подключения конфигурации.htm");
	ФайлыДляПодстановкиПараметров.Добавить("VerInfo.txt");
	ФайлыДляПодстановкиПараметров.Добавить("ReadMe.txt");
	ФайлыДляПодстановкиПараметров.Добавить("Install.txt");
	ФайлыДляПодстановкиПараметров.Добавить("1cv8.mft");

	ВыполнитьПодстановкуПараметровВФайлах(КаталогФайловКомплектаУстановки, ФайлыДляПодстановкиПараметров);

	Лог.Информация("Окончание подготовки описания комплекта установки " + ТекущаяДата());

КонецПроцедуры

Процедура ПодготовитьОписаниеКомплектаОбновления()

	// Параметры["БазаПроверкиУстановки.КаталогВыгрузкиФайловКонфигурации"],
	Лог.Информация("Начало подготовки описания комплекта обновления " + ТекущаяДата());
	ФС.КопироватьСодержимоеКаталога(КаталогШаблоновКомплектаОбновления, КаталогФайловКомплектаОбновления);

	ФайлыДляПодстановкиПараметров = Новый Массив();
	ФайлыДляПодстановкиПараметров.Добавить("Финансовое планирование. Дополнение к 1СБухгалтерии государственного учреждения 2.0. Версия _МажорнаяВерсия_. Новое в версии.htm");
	ФайлыДляПодстановкиПараметров.Добавить("Методика обновления конфигурации.htm");
	ФайлыДляПодстановкиПараметров.Добавить("UpdInfo.txt");
	ФайлыДляПодстановкиПараметров.Добавить("ReadMe.txt");
	ФайлыДляПодстановкиПараметров.Добавить("1cv8upd.htm");

	ТаблицыДляПодстановкиПараметров = Новый Массив();
	ТаблицыДляПодстановкиПараметров.Добавить("Финансовое планирование. Дополнение к 1СБухгалтерии государственного учреждения 2.0. Версия _МажорнаяВерсия_. Изменения в версии.mxl");
	ТаблицыДляПодстановкиПараметров.Добавить("Финансовое планирование. Дополнение к 1СБухгалтерии государственного учреждения 2.0. Версия _МажорнаяВерсия_. Измененные объекты.mxl");

	ФайлыДляПереименования = Новый Массив();
	ФайлыДляПереименования.Добавить("Финансовое планирование. Дополнение к 1СБухгалтерии государственного учреждения 2.0. Версия _МажорнаяВерсия_. Новое в версии.htm"    );
	ФайлыДляПереименования.Добавить("Финансовое планирование. Дополнение к 1СБухгалтерии государственного учреждения 2.0. Версия _МажорнаяВерсия_. Изменения в версии.mxl");
	ФайлыДляПереименования.Добавить("Финансовое планирование. Дополнение к 1СБухгалтерии государственного учреждения 2.0. Версия _МажорнаяВерсия_. Измененные объекты.mxl");

	ВыполнитьПодстановкуПараметровВФайлах(КаталогФайловКомплектаОбновления, ФайлыДляПодстановкиПараметров);
	ВыполнитьПодстановкуПараметровВТаблицах(КаталогФайловКомплектаОбновления, ТаблицыДляПодстановкиПараметров);

	// ДобавитьСодержаниеТаблицы(
	// 	ОбъединитьПути(КаталогФайловКомплектаОбновления, "Финансовое планирование. Дополнение к 1СБухгалтерии государственного учреждения 2.0. Версия _МажорнаяВерсия_. Изменения в версии.mxl"),
	// 	ЗначенияПодстановки["ОтчетХранилища_ПоКомментариям"]
	// 	);
	// ДобавитьСодержаниеТаблицы(
	// 	ОбъединитьПути(КаталогФайловКомплектаОбновления, "Финансовое планирование. Дополнение к 1СБухгалтерии государственного учреждения 2.0. Версия _МажорнаяВерсия_. Измененные объекты.mxl"),
	// 	ЗначенияПодстановки["ОтчетХранилища_ПоОбъектам"]
	// 	);

	// Если ЗначениеЗаполнено(Параметры["КаталогРелизаЗакупок"]) Тогда

	// 	ДобавитьСодержаниеТаблицы(
	// 		ОбъединитьПути(КаталогФайловКомплектаОбновления, "Финансовое планирование. Дополнение к 1СБухгалтерии государственного учреждения 2.0. Версия _МажорнаяВерсия_. Изменения в версии.mxl"),
	// 		ОбъединитьПути(Параметры["КаталогРелизаЗакупок"], "Закупки. Дополнение к БГУ. Версия "+ Параметры["ВерсияЗакупок"] +" Изменения в версии.mxl")
	// 		);
	// 	ДобавитьСодержаниеТаблицы(
	// 		ОбъединитьПути(КаталогФайловКомплектаОбновления, "Финансовое планирование. Дополнение к 1СБухгалтерии государственного учреждения 2.0. Версия _МажорнаяВерсия_. Изменения в версии.mxl"),
	// 		ОбъединитьПути(Параметры["КаталогРелизаЗакупок"], "Закупки. Дополнение к БГУ. Версия "+ Параметры["ВерсияЗакупок"] +" Измененные объекты")
	// 		);
	
	// КонецЕсли;

	//ЗаполнитьНовоеВВерсии(КаталогФайловКомплектаОбновления, ТаблицыДляПодстановкиПараметров);

	ВыполнитьПереименованиеФайлов(КаталогФайловКомплектаОбновления, ФайлыДляПереименования);

	Лог.Информация("Окончание подготовки описания комплекта обновления " + ТекущаяДата());

КонецПроцедуры

Процедура ПодготовитьОписаниеКомплектаПоставки()

	// Параметры["БазаПроверкиУстановки.КаталогВыгрузкиФайловКонфигурации"],
	Лог.Информация("Начало подготовки описания комплекта поставки " + ТекущаяДата());
	ФС.КопироватьСодержимоеКаталога(КаталогШаблоновКомплектаУстановки, КаталогФайловКомплектаУстановки);

	ФайлыДляПодстановкиПараметров = Новый Массив();
	ФайлыДляПодстановкиПараметров.Добавить("Методика подключения конфигурации.htm");
	ФайлыДляПодстановкиПараметров.Добавить("VerInfo.txt");
	ФайлыДляПодстановкиПараметров.Добавить("ReadMe.txt");
	ФайлыДляПодстановкиПараметров.Добавить("Install.txt");
	ФайлыДляПодстановкиПараметров.Добавить("1cv8.mft");

	ВыполнитьПодстановкуПараметровВФайлах(КаталогФайловКомплектаУстановки, ФайлыДляПодстановкиПараметров);

	Лог.Информация("Окончание подготовки описания комплекта поставки " + ТекущаяДата());

КонецПроцедуры

////////////////////////////////////////////////////
// Подготовка

Функция НастроитьКонфигуратор(СтрокаПодключения, ИмяПользователя = Неопределено, ПарольПользователя = Неопределено, ИспользуемаяВерсияПлатформы = Неопределено) Экспорт
	
	Конфигуратор = Новый УправлениеКонфигуратором;
	Конфигуратор.КаталогСборки(КаталогВременныхФайлов());
	Конфигуратор.УстановитьКонтекст(СтрокаПодключения, ИмяПользователя, ПарольПользователя);
	Если ИспользуемаяВерсияПлатформы <> Неопределено Тогда
		Конфигуратор.ИспользоватьВерсиюПлатформы(ИспользуемаяВерсияПлатформы);
	КонецЕсли;

	Возврат Конфигуратор;
КонецФункции	

Функция ПрочитатьПараметрыКонфигураций()

	ПутьФайла = "E:\devops\git\JSON\conf.json";

	Парсер = Новый ПарсерАргументовКоманднойСтроки();

	ЧитательПараметров = Новый ЧитательПараметров;
	ЧитательПараметров.УстановитьФайлПоУмолчанию(ПутьФайла);

	ПараметрыКонфигураций = ЧитательПараметров.Прочитать(Парсер);

	Возврат ПараметрыКонфигураций;

КонецФункции

Функция ПрочитатьПараметры()

	ПутьФайла = "E:\devops\git\JSON\description.json";

	Парсер = Новый ПарсерАргументовКоманднойСтроки();

	ЧитательПараметров = Новый ЧитательПараметров;
	ЧитательПараметров.УстановитьФайлПоУмолчанию(ПутьФайла);

	Параметры = ЧитательПараметров.Прочитать(Парсер);

	Возврат Параметры;

КонецФункции

Процедура СоздатьКаталогиФайловПоставки()

	// Параметры["БазаПроверкиУстановки.КаталогВыгрузкиФайловКонфигурации"],
	Лог.Информация("Начало создния файлов описания поставки " + ТекущаяДата());
	
	КаталогРелиза = ОбъединитьПути(Параметры["КаталогРелизов"], Параметры["Версия"]);
	КаталогФайловКомплектаУстановки  = ОбъединитьПути(КаталогРелиза, "cf" );
	КаталогФайловКомплектаОбновления = ОбъединитьПути(КаталогРелиза, "cfu");
	КаталогФайловКомплектаПоставки   = ОбъединитьПути(КаталогРелиза, "zip");

	КаталогШаблонов = (Параметры["КаталогШаблонов"]);
	КаталогШаблоновКомплектаУстановки  = ОбъединитьПути(КаталогШаблонов, "cf" );
	КаталогШаблоновКомплектаОбновления = ОбъединитьПути(КаталогШаблонов, "cfu");
	КаталогШаблоновКомплектаПоставки   = ОбъединитьПути(КаталогШаблонов, "zip");

	ФС.ОбеспечитьПустойКаталог( КаталогРелиза                    );
	ФС.ОбеспечитьПустойКаталог( КаталогФайловКомплектаУстановки  );
	ФС.ОбеспечитьПустойКаталог( КаталогФайловКомплектаОбновления );
	ФС.ОбеспечитьПустойКаталог( КаталогФайловКомплектаПоставки   );

	Лог.Информация("Окончание создния файлов описания поставки " + ТекущаяДата());

КонецПроцедуры

Процедура ЗаполнитьЗначенияПодстановки()

	ЗначенияПодстановки = Новый Структура;
	ЗначенияПодстановки.Вставить("Версия", Параметры["Версия"]);
	ЗначенияПодстановки.Вставить("МажорнаяВерсия", Параметры["МажорнаяВерсия"]);
	ЗначенияПодстановки.Вставить("ВерсияСтрокой", СтрЗаменить(Параметры["Версия"], ".","_"));
	ЗначенияПодстановки.Вставить("ВерсияБГУ", 		Параметры["ВерсияБГУ"]);
	ЗначенияПодстановки.Вставить("ВерсияПлатформы", Параметры["ВерсияПлатформы"]);
	ЗначенияПодстановки.Вставить("ТаблицаПоддержкиОбъектовКонфигураций", СоставитьТаблицуПравилПоддержкиОбъектов(ПараметрыКонфигураций["БазаПоставки.КаталогВыгрузкиФайловКонфигурации"]));
	ЗначенияПодстановки.Вставить("КонфигурацииОбновления", СтрокаКонфигурацийОбноления());
	ЗначенияПодстановки.Вставить("ВерсииОбновления", СтрокаВерсийОбноления());
	ЗначенияПодстановки.Вставить("ДатаВыпускаОбновления", Формат(ТекущаяДата(),"ДЛФ=Д"));

	БазаРазработки = НастроитьКонфигуратор(
		Параметры["БазаПроверкиОбновленияОбщая.СтрокаПодключения"],
		Параметры["БазаПроверкиОбновленияОбщая.ИмяПользователя"],
		Параметры["БазаПроверкиОбновленияОбщая.ПарольПользователя"],

		Параметры["БазаПроверкиОбновленияОбщая.ВерсияПлатформы"]
		);

	//	СобратьОтчетПоХранилищу(БазаРазработки, "ПоКомментариям", "ОтчетХранилища_ПоКомментариям.mxl");
		//	СобратьОтчетПоХранилищу(БазаРазработки, "ПоОбъектам", "ОтчетХранилища_ПоОбъектам.mxl");
	//	ЗначенияПодстановки.Вставить("ОтчетХранилища_ПоКомментариям", ОбъединитьПути("E:\devops\tmp", "ОтчетХранилища_ПоКомментариям.mxl"));
	//	ЗначенияПодстановки.Вставить("ОтчетХранилища_ПоОбъектам", ОбъединитьПути("E:\devops\tmp","ОтчетХранилища_ПоОбъектам.mxl"));
	
КонецПроцедуры

////////////////////////////////////////////////////
// Таблица объектов

Функция ШапкаТаблицы()
	Результат = 
		"<TABLE style=""WIDTH: 952pt; BORDER-COLLAPSE: collapse"" cellSpacing=0 cellPadding=0 width=1269 border=0>
		|<COLGROUP>
		|<COL style=""WIDTH: 583pt; mso-width-source: userset; mso-width-alt: 28416"" width=777>
		|<COL style=""WIDTH: 115pt; mso-width-source: userset; mso-width-alt: 5595"" width=153>
		|<COL style=""WIDTH: 122pt; mso-width-source: userset; mso-width-alt: 5961"" width=163>
		|<COL style=""WIDTH: 132pt; mso-width-source: userset; mso-width-alt: 6436"" width=176>
		|<TBODY>
		|<TR style=""HEIGHT: 95.25pt; mso-height-source: userset"" height=127>
		|<TD class=xl67 style=""BORDER-TOP: #f0f0f0; HEIGHT: 95.25pt; BORDER-RIGHT: #f0f0f0; WIDTH: 583pt; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #aeaaaa"" height=127 width=777><FONT size=5><FONT face=Calibri>Имя объекта в дереве метаданных<SPAN style=""mso-spacerun: yes"">&nbsp;</SPAN></FONT></FONT></TD>
		|<TD class=xl68 style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; WIDTH: 115pt; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #aeaaaa"" width=153><FONT size=5><FONT face=Calibri>Снят с поддержки ФП<SPAN style=""mso-spacerun: yes"">&nbsp;</SPAN></FONT></FONT></TD>
		|<TD class=xl68 style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; WIDTH: 122pt; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #aeaaaa"" width=163><FONT size=5 face=Calibri>Редактируется с сохранением поддержки ФП</FONT></TD>
		|<TD class=xl68 style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; WIDTH: 132pt; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #aeaaaa"" width=176><FONT size=5><FONT face=Calibri>Редактируется с сохранением поддержки БГУ<SPAN style=""mso-spacerun: yes"">&nbsp;</SPAN></FONT></FONT></TD></TR>
		|<TR style=""HEIGHT: 25.5pt; mso-height-source: userset"" height=34>
		|<TD class=xl66 style=""BORDER-TOP: #f0f0f0; HEIGHT: 25.5pt; BORDER-RIGHT: #f0f0f0; WIDTH: 583pt; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent"" height=34 width=777></TD>
		|<TD class=xl66 style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; WIDTH: 115pt; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent"" width=153></TD>
		|<TD class=xl66 style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; WIDTH: 122pt; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent"" width=163></TD>
		|<TD class=xl66 style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; WIDTH: 132pt; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent"" width=176></TD></TR>
		|<TR style=""HEIGHT: 31.5pt"" height=42>
		|<TD class=xl66 style=""BORDER-TOP: #f0f0f0; HEIGHT: 31.5pt; BORDER-RIGHT: #f0f0f0; WIDTH: 583pt; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent"" height=42 width=777><SPAN style=""ORPHANS: 2; WIDOWS: 2; font-variant-ligatures: normal; font-variant-caps: normal; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial"">БухгалтерияГосударственногоУчреждения (корень дерева метаданных, устанавливать с ОТКЛЮЧЕННЫМ флагом ""Установить для подчиненных объектов"")</SPAN></TD>
		|<TD style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent""><FONT face=Calibri>+</FONT></TD>
		|<TD style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent""></TD>
		|<TD style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent""><FONT face=Calibri>+</FONT></TD></TR>
		|<TR style=""HEIGHT: 18.75pt"" height=25>
		|<TD class=xl65 style=""BORDER-TOP: #f0f0f0; HEIGHT: 18.75pt; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent"" height=25></TD>
		|<TD style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent""></TD>
		|<TD style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent""></TD>
		|<TD style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent""></TD></TR>
		|<TR style=""HEIGHT: 18.75pt"" height=25>
		|<TD class=xl69 style=""BORDER-TOP: #f0f0f0; HEIGHT: 18.75pt; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #d9d9d9"" height=25><STRONG><FONT size=5 face=Calibri>Общие</FONT></STRONG></TD>
		|<TD class=xl70 style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #d9d9d9""><FONT face=Calibri>&nbsp;</FONT></TD>
		|<TD class=xl70 style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #d9d9d9""><FONT face=Calibri>&nbsp;</FONT></TD>
		|<TD class=xl70 style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #d9d9d9""><FONT face=Calibri>&nbsp;</FONT></TD></TR>
		|<TR style=""HEIGHT: 15.75pt"" height=21>
		|<TD class=xl71 style=""BORDER-TOP: #f0f0f0; HEIGHT: 15.75pt; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #f2f2f2"" height=21><STRONG><FONT face=Calibri>Подсистемы</FONT></STRONG></TD>
		|<TD class=xl72 style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #f2f2f2""><FONT face=Calibri>&nbsp;</FONT></TD>
		|<TD class=xl72 style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #f2f2f2""><FONT face=Calibri>&nbsp;</FONT></TD>
		|<TD class=xl72 style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #f2f2f2""><FONT face=Calibri>&nbsp;</FONT></TD></TR>
		|<TR style=""HEIGHT: 15pt"" height=20>
		|<TD style=""BORDER-TOP: #f0f0f0; HEIGHT: 15pt; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent"" height=20><FONT face=Calibri>ФинансовоеПланирование</FONT></TD>
		|<TD style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent""></TD>
		|<TD style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent""><FONT face=Calibri>+</FONT></TD>
		|<TD style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent""></TD></TR>
		|<TR style=""HEIGHT: 15pt"" height=20>
		|<TD style=""BORDER-TOP: #f0f0f0; HEIGHT: 15pt; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent"" height=20><FONT face=Calibri>Закупки</FONT></TD>
		|<TD style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent""></TD>
		|<TD style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent""><FONT face=Calibri>+</FONT></TD>
		|<TD style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent""></TD></TR>
	|";
	
	Возврат Результат;

КонецФункции

Функция СтрокаТаблицы(Текст, Уровень, ОтметкаФП, ОтметкаБГУ)

	Если Уровень = 1 Тогда 
		Шаблон = "
		|<TR style=""HEIGHT: 18.75pt"" height=25>
		|<TD class=xl69 style=""BORDER-TOP: #f0f0f0; HEIGHT: 18.75pt; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #d9d9d9"" height=25><STRONG><FONT size=5 face=Calibri><Текст></FONT></STRONG></TD>
		|<TD class=xl70 style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #d9d9d9""><FONT face=Calibri>&nbsp;</FONT></TD>
		|<TD class=xl70 style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #d9d9d9""><FONT face=Calibri>&nbsp;</FONT></TD>
		|<TD class=xl70 style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #d9d9d9""><FONT face=Calibri>&nbsp;</FONT></TD></TR>
		|";
	ИначеЕсли Уровень = 2 Тогда
		Шаблон = "
		|<TR style=""HEIGHT: 15.75pt"" height=21>
		|<TD class=xl71 style=""BORDER-TOP: #f0f0f0; HEIGHT: 15.75pt; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #f2f2f2"" height=21><STRONG><FONT face=Calibri><Текст></FONT></STRONG></TD>
		|<TD class=xl72 style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #f2f2f2""><FONT face=Calibri>&nbsp;</FONT></TD>
		|<TD class=xl72 style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #f2f2f2""><FONT face=Calibri>&nbsp;</FONT></TD>
		|<TD class=xl72 style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: #f2f2f2""><FONT face=Calibri>&nbsp;</FONT></TD></TR>
		|";
	Иначе
		Шаблон = "
		|<TR style=""HEIGHT: 15pt"" height=20>
		|<TD style=""BORDER-TOP: #f0f0f0; HEIGHT: 15pt; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent"" height=20><FONT face=Calibri><Текст></FONT></TD>
		|<TD style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent""></TD>
		|<TD style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent""><FONT face=Calibri><ОтметкаФП></FONT></TD>
		|<TD style=""BORDER-TOP: #f0f0f0; BORDER-RIGHT: #f0f0f0; BORDER-BOTTOM: #f0f0f0; BORDER-LEFT: #f0f0f0; BACKGROUND-COLOR: transparent""><FONT face=Calibri><ОтметкаБГУ></FONT></TD></TR>
		|";
	КонецЕсли;

	Результат = СтрЗаменить(Шаблон, "<Текст>", Текст);
	Если ОтметкаФП Тогда
		Результат = СтрЗаменить(Результат, "<ОтметкаФП>", "+");
	Иначе 
		Результат = СтрЗаменить(Результат, "<ОтметкаФП>", "");
	КонецЕсли;
	Если ОтметкаБГУ Тогда
		Результат = СтрЗаменить(Результат, "<ОтметкаБГУ>", "+");
	Иначе 
		Результат = СтрЗаменить(Результат, "<ОтметкаБГУ>", "");
	КонецЕсли;
	
	Возврат Результат;

КонецФункции

Функция ПодвалТаблицы()
	Возврат "</TBODY></TABLE>";
КонецФункции

Функция ТипыОбщихОбъектов()

	Результат = Новый Массив;
	Результат.Добавить(Новый Структура("Имя, Представление", "ОбщийМодуль","Общие модули"));
	Результат.Добавить(Новый Структура("Имя, Представление", "ПараметрСеанса","Параметры сеанса"));
	Результат.Добавить(Новый Структура("Имя, Представление", "Роль","Роли"));
	Результат.Добавить(Новый Структура("Имя, Представление", "ОбщийРеквизит","Общие реквизиты"));
	Результат.Добавить(Новый Структура("Имя, Представление", "ПланОбмена","Планы обмена"));
	Результат.Добавить(Новый Структура("Имя, Представление", "КритерийОтбора","Критерии отбора"));
	Результат.Добавить(Новый Структура("Имя, Представление", "ПодпискаНаСобытие","Подписки на события"));
	Результат.Добавить(Новый Структура("Имя, Представление", "РегламентноеЗадание","Регламентные задания"));
	Результат.Добавить(Новый Структура("Имя, Представление", "ФункциональнаяОпция","Функциональные опции"));
	Результат.Добавить(Новый Структура("Имя, Представление", "ОпределяемыйТип","Определяемые типы"));
	Результат.Добавить(Новый Структура("Имя, Представление", "ОбщаяФорма","Общие формы"));
	Результат.Добавить(Новый Структура("Имя, Представление", "ОбщаяКоманда","Общие команды"));
	Результат.Добавить(Новый Структура("Имя, Представление", "ОбщийМакет","Общие макеты"));
	Результат.Добавить(Новый Структура("Имя, Представление", "ОбщаяКартинка","Общие картинки"));
	Результат.Добавить(Новый Структура("Имя, Представление", "ПакетXDTO","Пакеты XDTO"));
	Результат.Добавить(Новый Структура("Имя, Представление", "ЭлементСтиля","Элементы стиля"));

	Возврат Результат;

КонецФункции

Функция ТипыОбъектов()

	Результат = Новый Массив;
	Результат.Добавить(Новый Структура("Имя, Представление", "Константа","Константы"));
	Результат.Добавить(Новый Структура("Имя, Представление", "Справочник","Справочники"));
	Результат.Добавить(Новый Структура("Имя, Представление", "Документ","Документы"));
	Результат.Добавить(Новый Структура("Имя, Представление", "Перечисление","Перечисления"));
	Результат.Добавить(Новый Структура("Имя, Представление", "Отчет","Отчеты"));
	Результат.Добавить(Новый Структура("Имя, Представление", "Обработка","Обработки"));
	Результат.Добавить(Новый Структура("Имя, Представление", "ПланВидовХарактеристик","Планы видов характеристик"));
	Результат.Добавить(Новый Структура("Имя, Представление", "РегистрСведений","Регистры сведений"));
	Результат.Добавить(Новый Структура("Имя, Представление", "РегистрНакопления","Регистры накопления"));

	Возврат Результат;

	// СоответствиеИмен.Вставить("Задача"       , "Task"       );
	// СоответствиеИмен.Вставить("WebСервис"    , "WebService" );

	// СоответствиеИмен.Вставить("HTTPСервис"             ,  "HTTPService"         );
	// СоответствиеИмен.Вставить("ХранилищеНастроек"      ,  "SettingsStorage"     );
	// СоответствиеИмен.Вставить("ГруппаКоманд"           ,  "CommandGroup"        );
	// СоответствиеИмен.Вставить("ЖурналДокументов"       ,  "DocumentJournal"     );
	// СоответствиеИмен.Вставить("БизнесПроцесс"          ,  "BusinessProcess"     );
	// СоответствиеИмен.Вставить("ВнешнийИсточникДанных"  ,  "ExternalDataSource"  );
	
	// СоответствиеИмен.Вставить("ПараметрФункциональныхОпций" ,  "FunctionalOptionsParameter"  );
	// СоответствиеИмен.Вставить("ПланСчетов"                  ,  "ChartOfAccounts"             );
	// СоответствиеИмен.Вставить("РегистрБухгалтерии"          ,  "AccountingRegister"          );
	// СоответствиеИмен.Вставить("ПланВидовРасчета"            ,  "ChartOfCalculationTypes"     );
	// СоответствиеИмен.Вставить("РегистрРасчета"              ,  "CalculationRegister"         );

КонецФункции

Функция СоставитьТаблицуПравилПоддержкиОбъектов(КаталогФайловКонфигурации)

	ФормированиеНастроек = Новый ФормированиеНастроекОбъединения;
	ТаблицаОбъектов = ФормированиеНастроек.ТаблицаОбъектовПодсистем(
		КаталогФайловКонфигурации, 
		СтрРазделить("ФинансовоеПланирование, Закупки, ПриниматьОбъединением", ", "),
		СтрРазделить("ПриниматьОбъединением", ", "));

	Результат =	ШапкаТаблицы();

	Для каждого Тип Из ТипыОбщихОбъектов() Цикл
		СтрокиТипа = ТаблицаОбъектов.НайтиСтроки(Новый Структура("Тип", Тип.Имя));
		Если СтрокиТипа.Количество() > 0 Тогда
			Результат = Результат + СтрокаТаблицы(Тип.Представление, 2, ложь, ложь);
			Для Каждого стр Из СтрокиТипа Цикл
				Результат = Результат + СтрокаТаблицы(стр.Имя, 0, Истина, стр.ПрисутствуетВПодсистемах2);
			КонецЦикла;
		КонецЕсли;
	КонецЦикла;

	Для каждого Тип Из ТипыОбъектов() Цикл
		СтрокиТипа = ТаблицаОбъектов.НайтиСтроки(Новый Структура("Тип", Тип.Имя));
		Если СтрокиТипа.Количество() > 0 Тогда
			Результат = Результат + СтрокаТаблицы(Тип.Представление, 1, ложь, ложь);
			Для Каждого стр Из СтрокиТипа Цикл
				Результат = Результат + СтрокаТаблицы(стр.Имя, 0, Истина, стр.ПрисутствуетВПодсистемах2);
			КонецЦикла;
		КонецЕсли;
	КонецЦикла;

	Результат =	Результат + ПодвалТаблицы();

	Возврат Результат;

КонецФункции

///////////////////////////////////////////////////
// Подстановка

Процедура ВыполнитьПодстановкуПараметровВФайлах(Каталог, ФайлыДляПодстановкиПараметров)

	Для каждого Файл Из ФайлыДляПодстановкиПараметров Цикл
		Лог.Отладка("ВыполнитьПодстановкуПараметровВФайлах " + Файл);
		ВыполнитьПодстановкуПараметровВФайле(ОбъединитьПути(Каталог, Файл));
	КонецЦикла;

КонецПроцедуры

Процедура ВыполнитьПодстановкуПараметровВТаблицах(Каталог, ТаблицыДляПодстановкиПараметров)

	Для каждого Файл Из ТаблицыДляПодстановкиПараметров Цикл
		
		КопироватьФайл(		ОбъединитьПути(Каталог, Файл)	,
							"E:\devops\tmp\params.mxl"		);

		ЗапуститьВременнуюБазуНаВыполнениеОбработки("E:\devops\epf\ПодстановкаПараметровВТаблице.epf");

		КопироватьФайл(		"E:\devops\tmp\params.mxl"		,
							ОбъединитьПути(Каталог, Файл)	);

	КонецЦикла;

КонецПроцедуры

Процедура ВыполнитьПереименованиеФайлов(Каталог, ФайлыДляПереименования)

	СоответствиеИмен = Новый Соответствие;
	Для каждого Файл Из ФайлыДляПереименования Цикл
		
		СоответствиеИмен.Вставить(
			ОбъединитьПути(Каталог, Файл),
			ОбъединитьПути(Каталог, ВыполнитьПодстановкуПараметровВСтроке(Файл, "_", "_") )
			);

	КонецЦикла;

	Для каждого КЗ Из СоответствиеИмен Цикл
		ПереместитьФайл(КЗ.Ключ, КЗ.Значение);
	КонецЦикла;

КонецПроцедуры


Процедура ВыполнитьПодстановкуПараметровВФайле(Файл)

	ТекстФайла = ПолучитьТекстФайла(Файл);
	ТекстФайла = ВыполнитьПодстановкуПараметровВСтроке(ТекстФайла);
	УстановитьТекстФайла(Файл, ТекстФайла);

КонецПроцедуры

Функция ВыполнитьПодстановкуПараметровВСтроке(Знач Текст, НачалоПараметра = "<", КонецПараметра = ">")

	Для каждого КЗ Из ЗначенияПодстановки Цикл
		Текст = СтрЗаменить(Текст, НачалоПараметра + КЗ.Ключ + КонецПараметра, КЗ.Значение);
	КонецЦикла;

	Возврат Текст;

КонецФункции

Процедура ДобавитьСодержаниеТаблицы(КудаДобавить, ЧтоДобавить)

	КопироватьФайл(	КудаДобавить, "E:\devops\tmp\target.mxl");
	КопироватьФайл(	ЧтоДобавить,  "E:\devops\tmp\source.mxl");

	ЗапуститьВременнуюБазуНаВыполнениеОбработки("E:\devops\epf\ПрисоединитьОтчетХранилища.epf");

	КопироватьФайл(	"E:\devops\tmp\target.mxl", КудаДобавить );

КонецПроцедуры

///////////////////////////////////////////////////
// Служебные функции

Процедура ЗапуститьВременнуюБазуНаВыполнениеОбработки(ФайлОбработки)

	УправлениеКонфигуратором = Новый УправлениеКонфигуратором;
	УправлениеКонфигуратором.СоздатьФайловуюБазу(УправлениеКонфигуратором.ПутьКВременнойБазе());

	УправлениеКонфигуратором.ЗапуститьВРежимеПредприятия(,,"/Execute""" + ФайлОбработки + """");

	УправлениеКонфигуратором.УдалитьВременнуюБазу();

КонецПроцедуры

Функция СтрокаКонфигурацийОбноления()

	Результат = "конфигураци" + ?(Параметры.Количество() > 1, "й", "и");
	Результат = Результат + " верси" + ?(Параметры["ОбновляемыеВерсии"].Количество() > 1, "й ", "и ");
	Результат = Результат + СтрСоединить(Параметры["ОбновляемыеВерсии"], " ,");
	
	Возврат Результат;

КонецФункции

Функция СтрокаВерсийОбноления()

	Результат = СтрСоединить(Параметры["ОбновляемыеВерсии"], ";");
	
	Возврат Результат;

КонецФункции

Функция ПолучитьТекстФайла( Знач пИмяФайла )
	
	Лог.Отладка("ПолучитьТекстФайла " + пИмяФайла);
	чтениеТекста = Новый ЧтениеТекста(пИмяФайла, КодировкаТекста.UTF8);
	прочитанныйТекст = чтениеТекста.Прочитать();
	чтениеТекста.Закрыть();
	возврат прочитанныйТекст;

КонецФункции

Процедура УстановитьТекстФайла(Знач ПутьФайла, Знач ТекстФайла)
	Лог.Отладка("УстановитьТекстФайла " + ПутьФайла);
	ЗаписьТекста = Новый ЗаписьТекста(ПутьФайла);
	ЗаписьТекста.ЗаписатьСтроку(ТекстФайла);
	ЗаписьТекста.Закрыть();
КонецПроцедуры

Процедура СобратьОтчетПоХранилищу(Конфигуратор, ТипОтчета, ИмяФайла )
	ТипыГруппировкиОтчета = ?(ТипОтчета = "ПоОбъектам", 
		ТипыГруппировкиОтчетаПоВерсиямХранилища.ГруппировкаПоОбъектам, 
		ТипыГруппировкиОтчетаПоВерсиямХранилища.ГруппировкаПоКоммитам);

	Конфигуратор.ПолучитьОтчетПоВерсиямИзХранилища(

		"tcp://" + ПараметрыКонфигураций["БазаПроверкиОбновленияОбщая.Хранилище"],
		ПараметрыКонфигураций["БазаПроверкиОбновленияОбщая.ИмяПользователяХранилища"],
		ПараметрыКонфигураций["БазаПроверкиОбновленияОбщая.ПарольПользователяХранилища"],

		ИмяФайла,

		ПараметрыКонфигураций["Хранилище.НомерНачальнойВерсии"],
		ПараметрыКонфигураций["Хранилище.НомерКонечнойВерсии"],
		ТипыГруппировкиОтчета
		);


КонецПроцедуры

Лог = Логирование.ПолучитьЛог("oscript.tool.makeDescription");
ПарсерJSON = Новый ПарсерJSON;
Лог.УстановитьУровень(УровниЛога.Отладка);
//Лог.(ПарсерJSON.ЗаписатьJSON(Параметры));
//Лог.УстановитьУровень(УровниЛога.Отладка);
Параметры = ПрочитатьПараметры();
ПараметрыКонфигураций = ПрочитатьПараметрыКонфигураций();

СоздатьКаталогиФайловПоставки();
ЗаполнитьЗначенияПодстановки();
ПодготовитьОписаниеКомплектаУстановки();
ПодготовитьОписаниеКомплектаОбновления();
//ПодготовитьОписаниеКомплектаПоставки();
